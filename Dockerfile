FROM java:8

WORKDIR /

ADD graphhopper-web-3.0.jar graphhopper-web-3.0.jar
ADD vietnam-latest.osm.pbf vietnam-latest.osm.pbf
ADD config-example.yml config-example.yml
ADD ./graph-cache ./graph-cache

EXPOSE 8980

CMD ["java", "-Ddw.graphhopper.datareader.file=vietnam-latest.osm.pbf", "-jar", "graphhopper-web-3.0.jar", "server", "config-example.yml"]