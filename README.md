Move `Dockerfile` to directory `graphhopper`

Run GraphHopper

`sudo docker-compose up --scale graphhopper=4`

or
    
`pm2 restart vmap`

Run vAddress
    
`pm2 restart run-qa-vaddress`
